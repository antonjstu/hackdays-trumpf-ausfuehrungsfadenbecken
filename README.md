# Architecture

## Frontend (port 4200)
Angular Webapp. 
Premise: E57 scan files are located on the host system under the path /usr/share/data (which is then mapped into the backend container).\
**Step 1**: Specify file name (or path if your file is in /usr/share/data/folder/data.e57).\
**Step 2**: Specify whether you want the original image to be downsampled. If this option is not ticked, further processing steps will take noticably more time!\
**Step 3**: Cut the roof by lowering the maximum height using the slider. By pressing "Generate cross section" you can then preview what the factory hall will look like from a top view with every point removed over the specified threshold.\
**Step 4**: Specify the Z_Sat variable.\
**Step 5**: Done! The json file will be downloaded.

## Backend (port 8080)

Point cloud subsampling and preprocessing done in c++ using PCL and OpenCV.
Shape detection was done in python with OpenCV as well.
REST API written with FastAPI in python. Calls functions defined in C++ and Python.
Three endpoints:

- /convertImage/{image_path}/{shouldApplySubsampling} returns a rasterized image from the top view of the factory.
- /cutHeight/{height} cuts every point above the specified z-axis-value `height`.
- /exportJson/{height}/{z_sat} returns the json file in the given output format.

All Services are combined in the docker compose file (**NOT TESTED**).

## Möglichkeiten

Durch die Integration von OpenCV, PCL und dem E57Converter können diverse Verarbeitungsschritte auf den Eingangsdaten einfach in Frontend und Backend hinzugefügt werden. Dadurch können Anpassungen auch an suboptimale Scans oder an nicht ebene Böden nachimplementiert werden. Sowohl Frontend als auch Backend sind dabei erweiterbar und skalierbar in Docker Containern untergebracht und lassen sich somit an beliebige Szenarien anpassen (Weitere Filter oder andere Verarbeitungen) und die weitere Verarbeitung ist durch die implementierte Pipeline mit geringem Arbeitsaufwand umsetzbar.

## Bemerkungen

Da die benötigten Tools alle im docker container selbst gebaut werden (um die maximale Geschwindigkeit sicherzustellen), kann docker build je nach zur Verfügung stehender Hardware ungefähr eine Stunde zum Bauen brauchen. Frontend und Backend konnten wir leider nicht in der zur Verfügung stehenden Zeit komplett miteinander verbinden, die implementierten Befehle lassen sich durch geringe Anpassungen (Stand 18.10 18:00 Uhr) lauffähig machen. Leider konnten wir die auf Grund der durch das nicht Funktionieren der CLI von CloudCompare verlorene Zeit nicht wieder aufholen und die Befehle sind so wie sie aktuell implementiert sind leider unter den für uns verfügbaren Hardware-Bedingungen nicht lauffähig. Die entworfene Architektur ist allerdings in hohem Maße erweiterbar und kann für die Implementierung weiterer Features genutzt werden.

# Ideas

## Was ist ein Obstacle
Objekt im Raum (nicht Wand) welches potenziell Satellitensignale abfängt oder reflektiert. Derzeit nicht implementiert mit Starthöhe. Potenziell Starthöhe in Json kodieren.

## Veranschaulichung Punkte
Rausschneiden und neue Wände ziehen?

## Development in Docker Image
Python Docker Image
Python Flask

## Pipeline
### 3D Filter
Wände identifizieren, extrahieren.
Obstacles als zylinderartige Objekte im Raum.

Wie?
Direkt am Anfang Dimension reduzieren?

z_sat variabel

Unvollständige Daten/ Schlechte Scans?
- Punkte die nicht scanbar waren sind Obstacles
- Hängende Obstacles problematisch

### 2D Muster Erkennung
### Export von 2D in JSON


# Fragen
- z_sat max höhe oder genaue Höhe? -> maximale Satellitenhöhe
- z_marker was ist das? -> uns egal
- Sind die Scans vollständig und haben alle wichtigen Flächen abgescant? Oder muss man implizit Flächen annehmen, welche evtl nicht gescant wurden? (ZB auf einer Maschine, wenn nur auf Kopfhöhe gescant)

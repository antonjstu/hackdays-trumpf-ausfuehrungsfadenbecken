import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-pipeline',
  templateUrl: './pipeline.component.html',
  styleUrls: ['./pipeline.component.scss']
})
export class PipelineComponent implements OnInit {

  @ViewChild('imageView') imageView: ElementRef;
  @ViewChild('stepper') stepper: MatStepper;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  image;
  loadingFirst: boolean;
  heightInCm: number;

  constructor(
    private _formBuilder: FormBuilder,
    private http: HttpClient,
    private sanitizer: DomSanitizer
    ) { }

  ngOnInit(): void {
    this.loadingFirst = false;
    this.firstFormGroup = this._formBuilder.group({
      filename: ['', Validators.required, ]
    });
    this.secondFormGroup = this._formBuilder.group({
      enableSubsampling: new FormControl(true),
    });
    this.thirdFormGroup = this._formBuilder.group({
      heightValue: new FormControl(this.heightInCm),
      zsat: new FormControl(4.5),
    });
  }

  async onSubmitE57() {
    this.loadingFirst = true;
    if (this.firstFormGroup.invalid || this.secondFormGroup.invalid) {
      return;
    }
    //this.image = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/ong;base64,' + image);
    const response = await this.http.get<any>(`http://localhost:8080/convertImage/${this.firstFormGroup.controls.filename.value}/${this.secondFormGroup.controls.enableSubsampling.value}`, {observe: 'response'}).toPromise();
    if (response.status == 200) {
      this.loadingFirst = false;
      this.image = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' +  response.body.image);
      this.heightInCm = response.body.height;
    } else {
      this.stepper.reset();
      console.error('Something went wrong during processing of image.');
    }
  }

  async onSubmitHeight() {
    console.log('Updating height...', this.thirdFormGroup.controls.heightValue.value);
    this.image = undefined;
    this.loadingFirst = true;
    let height = this.thirdFormGroup.controls.heightValue.value != null ? this.thirdFormGroup.controls.heightValue.value : this.heightInCm;
    const response = await this.http.get<any>(`http://localhost:8080/cutHeight/${height}`, {observe: 'response'}).toPromise();
    if (response.status == 200) {
      this.loadingFirst = false;
      this.image = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64,' +  response.body.image);
      this.heightInCm = response.body.height;
    } else {
      console.error('Something went wrong.');
      this.stepper.reset();
    }
  }

  async onSubmitZsat() {
    this.image = undefined;

    this.http.get(`http://localhost:8080/exportJson/${this.thirdFormGroup.controls.heightValue.value}/${this.thirdFormGroup.controls.zsat.value}`, {responseType: "blob"})
    .toPromise()
    //.catch(err => console.log('Something went wrong downloading the file'))
    .then(res => {
      return {
        "filename": "output.json",
        "data": res
      }
    }).then(data=> {
      let url = (window.URL ? URL : webkitURL).createObjectURL(data.data);
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = data.filename;
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    });
    console.log(this.thirdFormGroup.controls.heightValue.value);
  }

  formatLabel(value: number) {
    return (value / 100).toFixed(1) + "m";
  }

  rotateImage(): boolean {
    let el = document.getElementById('crossSectionImg');
    if (!el) {
      return false;
    }
    return el.clientWidth < el.clientHeight;
  }
}

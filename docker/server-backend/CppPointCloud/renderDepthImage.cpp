#include <bits/stdc++.h>
#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>
#include <pcl/cloud_iterator.h>
#include <pcl/common/common.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <sstream>
#include <string>

// cd ~/hackathon/hackdays-trumpf-ausfuehrungsfadenbecken/CppPointCloud/ && rm
// -rf build && mkdir build && cd build && cmake .. && make -j 3 && ./render2d

pcl::PointCloud<pcl::PointXYZ>::Ptr
passThroughFilter1D(const pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                    const std::string field, const double low,
                    const double high, const bool remove_inside) {
  if (low > high) {
    std::cout << "Warning! Min is greater than max!\n";
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered(
      new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PassThrough<pcl::PointXYZ> pass;

  pass.setInputCloud(cloud);
  pass.setFilterFieldName(field);
  pass.setFilterLimits(low, high);
  pass.setFilterLimitsNegative(remove_inside);
  pass.filter(*cloud_filtered);
  return cloud_filtered;
}

cv::Mat makeImageFromPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
                                std::string dimensionToRemove, float stepSize1,
                                float stepSize2, double maxH) {
  pcl::PointXYZ cloudMin, cloudMax;
  pcl::getMinMax3D(*cloud, cloudMin, cloudMax);

  std::string dimen1, dimen2;
  float dimen1Max, dimen1Min, dimen2Min, dimen2Max;
  if (dimensionToRemove == "x") {
    dimen1 = "y";
    dimen2 = "z";
    dimen1Min = cloudMin.y;
    dimen1Max = cloudMax.y;
    dimen2Min = cloudMin.z;
    dimen2Max = cloudMax.z;
  } else if (dimensionToRemove == "y") {
    dimen1 = "x";
    dimen2 = "z";
    dimen1Min = cloudMin.x;
    dimen1Max = cloudMax.x;
    dimen2Min = cloudMin.z;
    dimen2Max = cloudMax.z;
  } else if (dimensionToRemove == "z") {
    dimen1 = "x";
    dimen2 = "y";
    dimen1Min = cloudMin.x;
    dimen1Max = cloudMax.x;
    dimen2Min = cloudMin.y;
    dimen2Max = cloudMax.y;
  }

  std::vector<std::vector<int>> pointCountGrid;
  int maxPoints = 0;

  std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> grid;

  for (float i = dimen1Min; i < dimen1Max; i += stepSize1) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr slice =
        passThroughFilter1D(cloud, dimen1, i, i + stepSize1, false);
    grid.push_back(slice);

    std::vector<int> slicePointCount;

    for (float j = dimen2Min; j < dimen2Max; j += stepSize2) {
      pcl::PointCloud<pcl::PointXYZ>::Ptr grid_cell =
          passThroughFilter1D(slice, dimen2, j, j + stepSize2, false);
      float maxVal = -1000;
      for (const auto &elem : grid_cell->points) {
        if (maxH > 0 && elem.z > maxH)
          continue;
        if (elem.z > maxVal) {
          maxVal = elem.z;
        }
      }
      slicePointCount.push_back(maxVal);
    }
    pointCountGrid.push_back(slicePointCount);
  }

  cv::Mat mat(static_cast<int>(pointCountGrid.size()),
              static_cast<int>(pointCountGrid.at(0).size()), CV_8UC1);
  mat = cv::Scalar(0);

  for (int i = 0; i < mat.rows; ++i) {
    for (int j = 0; j < mat.cols; ++j) {
      int pointCount = pointCountGrid.at(i).at(j);

      mat.at<uchar>(i, j) = pointCount;
    }
  }

  return mat;
}

int main(int argc, char **argv) {
  if (argc <= 2) {
    std::cout << "no filepaths specified. exiting." << std::endl;
    return -1;
  }
  double maxH = -1;
  if (argc > 2) {
    std::stringstream ss;
    ss << argv[3];
    ss >> maxH;
  }
  std::string inputCloudFile(argv[1]);
  std::string outPath(argv[2]);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  if (pcl::io::loadPCDFile<pcl::PointXYZ>(inputCloudFile, *cloud) ==
      -1) //* load the file
  {
    PCL_ERROR("Couldn't read file test_pcd.pcd \n");
    return (-1);
  }
  // std::cout << "Loaded w:" << cloud->width << " h:" << cloud->height
  //           << std::endl;
  const float stepSize1 = 0.01;
  const float stepSize2 = 0.01;
  auto resImg = makeImageFromPointCloud(cloud, "z", stepSize1, stepSize2, maxH);
  double min, max;
  cv::minMaxLoc(resImg, &min, &max);
  std::cout << max;

  std::fstream file;
  file.open(outPath + ".txt", std::ios::out);

  for (int x = 0; x <= resImg.rows; x++) {
    for (int y = 0; y <= resImg.cols; y++) {
      std::stringstream ss;
      ss <<resImg.at<float>(y,x);
      file << ss.rdbuf() << " ";
    }
    file << "\n";
  }
  file.close();
  cv::imwrite(outPath+".png", resImg);
  return (0);
}
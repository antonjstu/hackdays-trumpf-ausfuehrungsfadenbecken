#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/opencv.hpp>
#include <pcl/common/common.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <string>
#include <pcl/cloud_iterator.h>

int main(int argc, char **argv) {
  if (argc <= 2) {
    std::cout << "no filepaths specified. exiting." << std::endl;
    return -1;
  }
  std::string inputCloudFile(argv[1]);
  std::string outPath(argv[2]);
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
  if (pcl::io::loadPCDFile<pcl::PointXYZ>(inputCloudFile, *cloud) ==
      -1) //* load the file
  {
    PCL_ERROR("Couldn't read file test_pcd.pcd \n");
    return (-1);
  }
pcl::PointXYZ min_cl;
pcl::PointXYZ max_cl;
  std::cout << "Loaded w:" << cloud->width << " h:" << cloud->height << "boundingbox: h:" << cloud->height << " w:" << cloud->width << " d:" 
            << std::endl;
  const float stepSize1 = 0.01;
  const float stepSize2 = 0.01;
  
  return (0);
}
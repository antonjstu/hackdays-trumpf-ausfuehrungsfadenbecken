import cv2, readMatrix, numpy, json, os, argparse, sys


# Add command line arguments
def parseArgs(parser):
    parser.add_argument("--unit", help="The unit of the color values in the height map.", type=str, nargs='?', default='m')
    parser.add_argument("--ceiling_height", help="The height of the ceiling.", type=float, nargs='?', default=0.0)
    parser.add_argument("--satellite_height", help="The desired height of the satellites.", type=float, required=True)
    parser.add_argument("--marker_height", help="The height of the markers.", type=int, nargs='?', default=1)
    parser.add_argument("--optimize", help="Activate/deactivate optimization.", action="store_true")
    parser.add_argument("--marker_grid", help="...", type=int, nargs='?', default=1)
    parser.add_argument("--satellite_grid", help="...", type=int, nargs='?', default=10)
    parser.add_argument("--image_x_scale", help="The width of the image in meters.", type=float, required=True)
    parser.add_argument("--image_y_scale", help="The height of the image in meters.", type=float, required=True)


# Find contours in image and create rectangles
def getRectsFromImage(image):
    print("Finding contours..")
    #gaussed = cv2.GaussianBlur(image, (3, 3), 0)
    #cv2.imshow("gauss", gaussed)
    gray = cv2.bilateralFilter(image, 11, 17, 17)
    cv2.imshow("bilateral", gray)
    cv2.imwrite("out/bilateral.png", gray)
    kernel = numpy.ones((3, 3), numpy.uint8)
    eroded = cv2.erode(gray, kernel, iterations=1)
    cv2.imshow("eroded", eroded)
    cv2.imwrite("out/eroded.png", eroded)
    kernel = numpy.ones((2, 2), numpy.uint8)
    dilated = cv2.dilate(eroded, kernel, iterations=1)
    cv2.imshow("dilated", dilated)
    cv2.imwrite("out/dilated.png", dilated)
    imageEdged = cv2.Canny(dilated, 30, 200)
    cv2.imshow("canny", imageEdged)
    cv2.imwrite("out/canny.png", imageEdged)
    contours, hierarchy = cv2.findContours(imageEdged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # Create and sort rectangles
    rectangles = [cv2.boundingRect(cnt) for cnt in contours]
    print("Rectangles found: ", len(rectangles))
    return rectangles


# Iterate over rectangle in height map and evaluate greatest value
def calculateRectangleHeight(heightMap, rectangle):
    maxValue = 0.0
    xStart = rectangle[2] if rectangle[0] > rectangle[2] else rectangle[0]
    yStart = rectangle[3] if rectangle[1] > rectangle[3] else rectangle[1]
    xEnd = rectangle[0] if rectangle[0] > rectangle[2] else rectangle[2]
    yEnd = rectangle[1] if rectangle[1] > rectangle[3] else rectangle[3]
    for y in range(yStart, yEnd):
        for x in range(xStart, xEnd):
            if heightMap[y, x, 0] > maxValue:
                maxValue = heightMap[y, x, 0] # since heightMap is a grayscale, in all color channels are equal values
    return maxValue


# Draw the rectangles on an image
def drawRectsOnImage(image, rectangles):
    # Draw rectangles on image
    imageWithRectangles = image.copy()
    [cv2.rectangle(imageWithRectangles, rectangle, (0, 255, 0), 1, 8, 0) for rectangle in rectangles]
    return imageWithRectangles


# Create and append the entries for "layers" in json
def appendRectanglesToLayers(heightMap, rectangles, arguments):
    shapeId = 1
    layers = list()
    # TODO: convert pixel coords to meters
    for rectangle in rectangles:
        id = 1
        entry = dict()
        entry["points"] = list()
        points = list()
        points.append((rectangle[0], rectangle[1]))
        points.append((rectangle[2], rectangle[1]))
        points.append((rectangle[0], rectangle[3]))
        points.append((rectangle[2], rectangle[3]))
        for p in points:
            entry["points"].append({
                "x": p[0] / heightMap.shape[1] * arguments.image_x_scale,
                "y": p[1] / heightMap.shape[0] * arguments.image_y_scale,
                "id": id})
            id += 1
        entry["height"] = float(calculateRectangleHeight(heightMap, rectangle))
        entry["shape_type"] = "obstacle" # TODO: Detect walls
        entry["shapeId"] = str(shapeId)
        layers.append(entry)
        shapeId += 1
    return layers


def appendRectanglesToShapes(heightMap, rectangles):
    shapeId = 1
    shapes = list()
    for rectangle in rectangles:
        entry = dict()
        entry["userInput"] = calculateRectangleHeight(heightMap, rectangle)
        entry["x"] = rectangle[0]
        entry["y"] = rectangle[1]
        entry["width"] = rectangle[2] - rectangle[0]
        entry["height"] = rectangle[3] - rectangle[1]
        entry["rotation"] = 0
        entry["sId"] = str(shapeId)
        entry["sType"] = "rect"
        entry["obstacleType"] = "obstacle" # TODO: Detect walls
        entry["stroke"] = "black"
        entry["strokeWidth"] = 3
        entry["cursor"] = "pointer"
        entry["fill"] = "#808080"
        entry["opacity"] = 0.6
        shapes.append(entry)
        shapeId += 1
    return shapes


# Write the json to file
def writeJson(dictionary, outPath, outName):
    if not os.path.exists(outPath):
        os.makedirs(outPath)
    jsonDict = json.dumps(dictionary)
    filepath = os.path.join(outPath, outName + ".json")
    f = open(filepath, "w+")
    f.write(jsonDict)
    f.close()


# Create json output file
def createJson(heightMap, args, rectangles):
    output = dict()
    output["unit"] = args.unit
    output["z_ceil"] = args.ceiling_height
    output["z_sat"] = args.satellite_height
    output["z_marker"] = args.marker_height
    output["layers"] = appendRectanglesToLayers(heightMap, rectangles, args)
    output["optimize"] = args.optimize
    output["marker_grid"] = args.marker_grid
    output["sat_grid"] = args.satellite_grid
    return output


def createTestJson(heightMap, args, rectangles):
    output = dict()
    output["unit"] = args.unit
    output["z_ceil"] = args.ceiling_height
    output["z_sat"] = args.satellite_height
    output["z_marker"] = args.marker_height
    output["allShapes"] = appendRectanglesToShapes(heightMap, rectangles)
    output["layers"] = appendRectanglesToLayers(heightMap, rectangles)
    output["optimize"] = args.optimize
    output["marker_grid"] = args.marker_grid
    output["sat_grid"] = args.satellite_grid
    output["marker_height"] = 1
    output["scale"] = {"convertVal": 0.02177930635740224, "unit": args.unit}
    output["ratio"] = 0.6956132497761862
    output["shapeScale"] = {
        "points": (705.26844, 9.2088552, 2574.014366, 10.5244059),
        "userInput": 40.7,
        "x": -102.5496,
        "y": 1.0259,
        "rotation": 0,
        "closed": False,
        "sType": "Line",
        "stroke": "black",
        "strokeWidth": 5,
        "cursor": "pointer",
        "draggable": True,
        "noCache": False}
    output["imgW"] = 114 # TODO ?
    #output["img"] = "data:image/png;base64," + base64.b64encode(heightMap).decode("ascii") # TODO: fix
    return output


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Use this script to detect obstacles in a height map.")
    parseArgs(parser)
    arguments = parser.parse_args()

    # Read image
    heightMap = readMatrix.read("in/raster_matrix.txt")
    byteHeightMap = readMatrix.floatToByte(heightMap)
    cv2.imwrite("out/input.png", byteHeightMap)

    # Detect and draw rectangles
    rectangles = getRectsFromImage(byteHeightMap)
    rectsOnImage = drawRectsOnImage(byteHeightMap, rectangles)

    jsonDict = createJson(heightMap, arguments, rectangles)

    cv2.imshow("image", rectsOnImage)
    cv2.imwrite("out/output.png", rectsOnImage)

    writeJson(jsonDict, "out", "result")

    if cv2.waitKey(0) & 0xFF == 27:
        cv2.destroyAllWindows()

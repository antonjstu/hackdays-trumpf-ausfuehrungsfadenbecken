import numpy as np


def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def file_items_per_row(fname):
    with open(fname) as f:
        line = f.readline()
        return(len(line.strip().split(" ")))


def floatToByte(floatImage):
    byteImage = np.empty_like(floatImage, dtype=np.uint8)
    np.copyto(byteImage, floatImage / np.amax(floatImage) * 255, casting="unsafe")
    return byteImage


def read(filename):
    heightMatrix = np.empty([file_len(filename), file_items_per_row(filename), 3])
    # loading values
    with open(filename, "r") as f:
        for x, line in enumerate(f):
            for y, val in enumerate(line.strip().split(" ")):
                # print((x, y))
                heightMatrix[x, y, 0] = float(val)
                heightMatrix[x, y, 1] = float(val)
                heightMatrix[x, y, 2] = float(val)

    minValue = np.amin(heightMatrix)
    maxValue = np.amax(heightMatrix)
    print(f"MatrixShape: {heightMatrix.shape}")
    print(f"MinValue: {minValue}")
    print(f"MaxValue: {maxValue}")

    # Convert values in array from 0 to minValue+maxValue
    heightMatrix += abs(minValue)

    return heightMatrix

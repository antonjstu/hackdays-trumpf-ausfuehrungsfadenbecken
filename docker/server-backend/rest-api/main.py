# from typing import Optional

from typing import List
from fastapi import FastAPI,HTTPException
import os.path
import base64
import json
import os
from subprocess import CalledProcessError
import subprocess
from fastapi.responses import JSONResponse, StreamingResponse
from shutil import copyfile


app = FastAPI()
base_path = "/usr/share/data/"
image_file = ""
max_height = 0.0

@app.get("/convertImage/{path}/{isSubsample}")
def convert_image(path: str, isSubsample: bool):
    image_file = path
    completepath = os.path.normpath(base_path + path)
    outpath = os.path.normpath(base_path + "return")
    if not os.path.isfile(completepath):
        raise HTTPException(status_code=500, detail="Given path could not be found" + completepath)

    args = ["/CppPointCloud/build/render2d", completepath, outpath]

    max_height = subprocess.check_output(args, universal_newlines=True)

    #assert that path exists
    if not os.path.isfile(outpath):
        raise HTTPException(status_code=500, detail="Cpp did not return valid image path!")

    data = open(os.path.normpath(base_path + "return.png"), 'rb').read()
    enc = base64.b64encode(data)

    return JSONResponse(content={"image": enc.decode("utf-8") , "height": max_height})

@app.get("/cutHeight/{maxHeight}")
def convert_image(maxHeight: float):
    completepath = os.path.normpath(base_path + image_file)
    outpath = os.path.normpath(base_path + "return.png")
    if not os.path.isfile(completepath):
        raise HTTPException(status_code=500, detail="Given path could not be found" + completepath)

    args = ["/CppPointCloud/build/render2d", completepath, outpath, maxHeight]

    try:
        console_output = subprocess.check_output(args, universal_newlines=True)
    except CalledProcessError:
        raise HTTPException(status_code=500, detail="cpp failed!")

    # /opt/pcl/release/bin
    print(console_output)
    # dict = json.loads(console_output)
    returned_path = outpath
    # scalar = dict['scalar']
    scalar = 0

    #assert that path exists
    if not os.path.isfile(returned_path):
        raise HTTPException(status_code=500, detail="Cpp did not return valid image path!")

    data = open(returned_path, 'rb').read()
    enc = base64.b64encode(data)

    return JSONResponse(content={"image": enc.decode("utf-8") , "height": maxHeight})

@app.get("/exportJson/{height}/{z_sat}")
def export_json(height : float, z_sat : float):
    completepath = os.path.normpath(base_path + "return.txt")
    outpath = os.path.normpath("/python2d/out/result.json")
    if not os.path.isfile(completepath):
        raise HTTPException(status_code=500, detail="Given path could not be found" + completepath)

    copyfile(os.path.normpath(base_path + "return.txt"), "/python2d/in/raster_matrix.txt")
    copyfile(completepath, "/python2d/in/raster_image.png")
    args = ["/python2d/2dproenv/bin/python", "/python2d/detectRects.py", "--satellite_height", max_height,  "--image_x_scale", "19.7", "--image_y_scale",  "75.9", "--optimize" ]

    try:
        console_output = subprocess.check_output(args, universal_newlines=True)
    except CalledProcessError:
        raise HTTPException(status_code=500, detail="cpp failed!")

    print(console_output)
    # dict = json.loads(console_output)
    returned_path = outpath


    #assert that path exists
    if not os.path.isfile(returned_path):
        raise HTTPException(status_code=500, detail="Internal server Error!")

    data = open(returned_path).read()

    # get json String 
    json = data
    file = open("output.json", "w")
    file.write(json)
    return StreamingResponse(json, media_type="blob")
#!/bin/sh
cd rest-api/ 
. apienv/bin/activate
uvicorn main:app --port 8080 --host 0.0.0.0